# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2021/01/04 14:48:35 by lorenuar          #+#    #+#              #
#    Updated: 2021/02/20 18:22:43 by lorenuar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#************************* NASM Assembly - 64 bits ****************************#

NAME = libasm.a

SHELL = /bin/bash

AR = ar
ARFLAGS = -rcs

AS = nasm
ASFLAGS = -f elf64
# ASFLAGS += -g -F dwarf

LD = ld

CC = gcc
LDFLAGS = $(NAME)

SRCDIR	= src/
INCDIR	= includes/
OBJDIR	= bin/

CFLAGS += -I $(INCDIR)

TESTFILE = main.c
TESTOBJ = $(TESTFILE:.c=.o)

EXECNAME = exec.out

GR	= \033[32;1m#	Green
RE	= \033[31;1m#	Red
YE	= \033[33;1m#	Yellow
CY	= \033[36;1m#	Cyan
RC	= \033[0m#	Reset Colors

###▼▼▼<src-updater-do-not-edit-or-remove>▼▼▼
# **************************************************************************** #
# **   Generated with https://github.com/lorenuars19/makefile-src-updater   ** #
# **************************************************************************** #

SRCS = \
	./src/ft_write.s \
	./src/ft_strdup.s \
	./src/ft_strcmp.s \
	./src/ft_strlen.s \
	./src/ft_strcpy.s \
	./src/ft_read.s \

HEADERS = \
	./includes/libasm.h \

###▲▲▲<src-updater-do-not-edit-or-remove>▲▲▲

SRC		:= $(notdir $(SRCS)) # 				Files only
OBJ		:= $(SRC:.s=.o)	#					Files only
OBJS	:= $(addprefix $(OBJDIR), $(OBJ)) #		Full path
SOBJS	:= $(addprefix $(SRCDIR), $(OBJ)) #		Full path
CSRCS	:= $(addprefix ../, $(SRCS)) #			Compiler

VPATH = $(SRCDIR):$(OBJDIR)

all : $(NAME)

$(OBJDIR)%.o : %.s
	@mkdir -p $(OBJDIR)
	@printf "$(GR)+ $(RC)"
	$(AS) $(ASFLAGS) $< -o $@

$(NAME) : $(SRCS) $(HEADERS) $(OBJS)
	@printf "\n$(GR)=== Compiled [$(AS) $(ASFLAGS)] ===\n$(SRC)\n"
	@printf "$(GR)=== Archiving [$(AR) $(ARFLAGS)] to $(NAME) ===$(RC)\n"
	@$(AR) $(ARFLAGS) $(NAME) $(OBJS)

clean :
	@printf "$(RE)--- Removing $(OBJS)$(RC)\n"
	@rm -rf $(OBJDIR)

fclean : clean
	@printf "$(RE)--- Removing $(NAME)$(RC)\n"
	@rm -rf $(NAME) $(EXECNAME)

re : fclean all

$(EXECNAME) : $(TESTFILE) $(NAME)
	@printf "$(GR)+++ compiling $(TESTFILE) into $(TESTOBJ) +++$(RC)\n"
	$(CC) $(CFLAGS) $(TESTFILE) $(NAME) -o $(EXECNAME)

run : $(EXECNAME)
	@printf "$(CY)>>> Running $(EXECNAME)$(RC)\n"
	@./$(EXECNAME)

.PHONY : all clean fclean re run
