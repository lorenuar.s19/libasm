#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include "libasm.h"

#define PRINT_DETAILS(str) print_details(ft_strlen(str), str, #str)

void	print_details(size_t len, char *str, const char *name)
{
	len++;
	printf("[%s][%p]Details (%ld): \n[", name, str, len);
	while (len > 0)
	{
		if (*str >= ' ' && *str < '~')
		{
			printf("%c", *str);
		}
		else
		{
			printf("\\%d", *str);
		}
		str++;
		len--;
		if (len > 0)
		{
			printf("|");
		}
	}
	printf("] END_DETAILS\n");
}

#define BUFF_SIZE	50

void		put_str(char *str)
{
	ft_write(1, str, ft_strlen(str));
}

#define BUF_SIZ 1024

int			main(void)
{
	char buffer[BUF_SIZ];
	char print[BUF_SIZ];
	char *ans1;
	char *ans2;
	int	ret;

	put_str("\nHello, please enter your name :\n");

	ret = ft_read(2, buffer, BUF_SIZ);
	buffer[ret] = '\0';

	ans1 = ft_strdup(buffer);

	put_str("Hello, please enter the same name :\n");

	ret = ft_read(2, buffer, BUF_SIZ);
	buffer[ret] = '\0';

	ans2 = ft_strdup(buffer);

	if (ft_strcmp(ans1, ans2) == 0)
	{
		ft_strcpy(print, "Answers are the same\n");
	}
	else
	{
		ft_strcpy(print, "\n\033[31;1m!ERROR! Answers are different !ERROR!\033[0m\n");
		put_str(print);
		return (1);
	}
	put_str(print);
	put_str("\nHave a nice day ");
	put_str(ans1);
	put_str("\n");

	return (0);
}
