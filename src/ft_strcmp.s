;	lorenuar
;
;	int	ft_strcmp(char *s1, char *s2);
;	              RDI     , RSI

section .text
global ft_strcmp

ft_strcmp:
	xor rax, rax	; clear RAX
.test_s1_null:
	test rdi, rdi	; test RDI for NULL
	jz .return		; jmp if ZERO
.test_s2_null:
	test rsi, rsi	; test RSI for NULL
	jz .return		; jmp if ZERO
.cmp_char:
	xor rdx, rdx	; clear RDX
	xor rcx, rcx	; clear RCX
	mov dl, byte [rdi]	; Copy s1 to DX
	movzx rdx, dl	; Extend sign DX to RDX
	mov cl, byte [rsi]	; copy s2 to CX
	movzx rcx, cl	; Extend sign CX to RCX
	cmp rcx, rdx		; compare CL with DL
	je .next_char	; jump if equal
	sub rdx, rcx
	jns .add_sub
.add_sub:
	add rax, rdx
	jmp .return
.next_char:
	cmp rcx, 0	; Compare CX with 0
	jz .return
	cmp rdx, 0	; Compare DX with 0
	jz .return
	inc rdi
	inc rsi
	jmp .cmp_char
.return:
	ret
