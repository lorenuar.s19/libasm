; lorenuar
;
; size_t		ft_strlen(char *str);
;
section .text
global ft_strlen

ft_strlen:
	mov rbx, rdi	; Copy first argument into RBX
	mov rax, rbx	; copy RBX into RAX

; .test_for_null:
; 	test rdi, rdi	; test if RDI is NULL == 0x0 == 0
; 	jz .null_arg	; jump if Zero flag

.loop:
	cmp byte [rax], 0	; compare each byte of the string with 0
	jz .end		; If cmp sets the zero flag, jump to .end
	inc rax		; Increment the counter
	jmp .loop	; loop back to .loop

; .null_arg:
; 	mov rax, -1		; set return to -1
; 	ret

.end:
	sub rax, rbx	; Subtract the address in RBX from the address in EAX
					; to get the length of the string
	ret
