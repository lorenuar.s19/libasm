;	lorenuar
;
;	char *strdup(const char *str);
;
section .text
global ft_strdup

extern ft_strlen
extern ft_strcpy

extern malloc

ft_strdup:
	call ft_strlen	; Get length
	push rdi		; preserve *str
	mov rdi, rax	; copy return form ft_strlen to RDI
	inc rdi			; increment RDI, to give space for the '\0'
	call malloc wrt ..plt	; malloc(RDI)
							;         ^- Return from ft_strlen + 1
	jc .return	; if malloc fails, it sets the CARRY flag
	pop rdi		; restore *str
	mov rsi, rdi	; put *s in RSI (2nd arg)
	mov rdi, rax	; put return from malloc in RDI (1st arg)
	call ft_strcpy	; ft_strcpy(RDI, RSI)
					;            ^    ^- input argument *str
					;            |- return from malloc
.return:
	ret
