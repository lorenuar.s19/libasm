;	lorenuar
;
;	char	*ft_strcpy(char *dst, char *src);
;	                   RDI      , RSI

section .text
global ft_strcpy

ft_strcpy:
	xor rax, rax	; XOR RAX with himself = reset to 0
	xor rcx, rcx	; XOR RCX with himself = reset to 0
	xor rdx, rdx	; XOR RDX with himself = reset to 0
.test_src_null:
	test rsi, rsi	; test RSI for NULL
	jz .return		; jump to .return if ZERO FLAG was set
.test_dst_null:
	test rdi, rdi	; test RDI for NULL
	jz .return		; jump to .return if ZERO FLAG was set
.test_src_equal:
	cmp rdi, rsi	; test if they are equal
	jne .copy_char	; jump if not equal
	mov rax, rsi	; RET
	ret
.copy_char:
	xor dl, dl					; clear DL
	mov dl, byte [rsi + rcx]	; Store char in DL register from SRC
	mov byte [rdi + rcx], dl	; Load char from DL register to DST
	inc rcx						; INCrement RAX
	cmp dl, 0					; Test DL to see if it is 0
	je .put_terminal_zero		; JUMP if ZERO flag
	jmp .copy_char				; LOOP
.put_terminal_zero:
	mov byte [rdi + rcx], 0		; Force putting a terminal '\0'
.set_return:
	mov rax, rdi				; Set RAX to RDI
.return:
	ret
