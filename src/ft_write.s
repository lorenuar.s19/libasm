; lorenuar
;
; ssize_t		ft_write(int fd, const void *buf, size_t n_bytes);
;
section .text
global ft_write

extern __errno_location

ft_write:
	mov rax, 1
	syscall
	cmp rax, 0
	jl .set_errno
	ret
.set_errno:
	neg rax
	mov rdi, rax
	call __errno_location wrt ..plt
	mov [rax], rdi
	mov	rax, -1
	ret
