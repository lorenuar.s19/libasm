; lorenuar
;
; ssize_t		fr_read(int fd, void *buf, size_t count);
;
section .text
global ft_read

extern __errno_location

ft_read:
	mov rax, 0		; set syscall (0 for read)
	syscall		; call read(RDI, RSI, RDX)
	cmp rax, 0		; check return of syscall
	jl .set_errno	; jump if less than
	ret
.set_errno:
	neg rax		; negate RAX
	mov rdi, rax	; copy RAX to RDI
	call __errno_location wrt ..plt		; call ERRNO
	mov [rax], rdi	; copy RDI into the value pointed by RAX
	mov	rax, -1		; set return to -1
	ret
