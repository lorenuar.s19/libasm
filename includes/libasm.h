/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libasm.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/23 01:24:52 by lorenuar          #+#    #+#             */
/*   Updated: 2021/02/20 18:13:46 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBASM_H
# define LIBASM_H

# include <stdlib.h>
# include <sys/types.h>
# include <errno.h>

extern size_t		ft_strlen(char *str);
extern char			*ft_strcpy(char *dst, char *src);
extern int			ft_strcmp(char *s1, char *s2);
extern char			*ft_strdup(const char *str);
extern ssize_t		ft_write(int fd, const void *buf, size_t n_bytes);
extern ssize_t		ft_read(int fd, void *buf, size_t count);

#endif
